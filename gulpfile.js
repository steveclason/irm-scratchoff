'use strict';

var gulp = require('gulp');
var postcss = require( 'gulp-postcss' );
var sourcemaps = require('gulp-sourcemaps');
var rename = require( 'gulp-rename' );
// var minify = require( 'gulp-minify-css' );
var autoprefixer = require( 'gulp-autoprefixer' );
var util = require( 'gulp-util' );
var log = util.log;
// var babel = require( 'gulp-babel' );
// require( 'stylelint' )(),

gulp.task('css', function () {
  log( 'Generate CSS files ' + (new Date()).toString());
  return gulp
    .src('./irm-scratchoff/css/_irm-scratchoff-style.css')
    .pipe(sourcemaps.init())
    .pipe(postcss([
      require( 'postcss-nesting' )
    ]))
    .pipe( autoprefixer( 'last 4 version' ))
    .pipe( rename( 'irm-scratchoff-style.css' ))
    .pipe( sourcemaps.write('.'))
    .pipe( gulp.dest( './irm-scratchoff/css' ));
  // .pipe( rename({ suffix: '.min' }))
  // .pipe(minify())
  // .pipe(gulp.dest( './tilghman' ));
});

// gulp.task( 'js', () =>
//   gulp.src('sots-charts/js/src/sots-charts.js')
//     .pipe(babel({
//       presets: ['@babel/preset-env']
//     }))
//     .pipe(gulp.dest('sots-charts/js'))
// );

// gulp.task('sass:watch', function () {
//   gulp.watch('./inn97win/sass/**/*.scss', ['sass']);
// });

// TODO Make a task to copy files to local WordPress.
// C:\xampp\apps\wordpress\htdocs

var watch = require('gulp-watch');

var source = './irm-scratchoff',  
  destination = 'C:/xampp/apps/wordpress/htdocs/wp-content/plugins/irm-scratchoff';

gulp.task('watch-folder', function() {  
  gulp.src( source + '/**/*', { base: source } )
    .pipe( watch( source, { base: source }))
    .pipe( gulp.dest( destination ));
});
