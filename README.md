# IRM-Scratchoff Plugin #

This MIT-licensed WordPress plugin displays three masked images which reveal an announcement when the user "scratches off" the masks. The three cards are all the same and every instance is a winner.

## What exactly have we got here? ##

* A client-specific WordPress plugin for Incite Response Digital Marketing Agency, [inciteresponse.com](https://inciteresponse.com).
* Version 1.0.0
* Copyright &copy; 2020, Incite Response, wScratchPad copyright &copy; 2011-2012 Websanova [http://www.websanova.com](http://www.websanova.com).
* License: [MIT](https://mit-license.org/)
* All code dependencies are included in the plugin, jQuery will be enqueued if it is not already.
* The plugin was written to work in a full-width container of the Avada theme, but can be used anywhere with modifications to the CSS.

## How do I use this? ##

### Manual Upload via WordPress Admin ###

1. Download the file [irm-scratchoff.zip](https://bitbucket.org/steveclason/irm-scratchoff/src/master/irm-scratchoff.zip)  from the repository at [https://bitbucket.org/steveclason/irm-scratchoff/src/master/](https://bitbucket.org/steveclason/irm-scratchoff/src/master/);

2. In your WordPress Dashboard, navigate to Plugins > Add New.
3. Click the Upload Plugin button at the top of the screen.
4. Select the zip file from your local filesystem.
5. Click the Install Now button.
6. When installation is complete, you’ll see “Plugin installed successfully.” Click the Activate Plugin button at the bottom of the page.
7. Navigate to the plugin settings at Dashboard > Settings > Scratchoff Options. The options are pre-populated with sane values but the settings __must be saved__ for the plugin to work the first time.
8. Include the shortcode [irm_scratchoff] on the WordPress page where you would like the feature to appear.
9. Modify your theme&apos;s style.css to match the plugin to your design.

### Plugin setup ###

* The plugin configuration screen can be found on the WordPress Dashboard at Settings > Scratchoff Options;
* Options are:
  1. Development Mode: provides some additional information and employs a non-minimized version of the wScratchpad library. Should normally be off.
  2. Background Image: used to select the image that is initially hidden and revealed on scratching, integrated with the WordPress Media Manager.
  3. Foreground Image: defaults to a silver pattern mimicking the ink used on scratch-cards. It can be changed to use any masking image, but some additional developer-level work is required to style a different image so it hasn't been integrated with the WordPress Media Manager.
  4. Completion Trigger: The percentage of each card that has to be scratched off before the entire card is revealed.
  5. Winner Text: The message that will be revealed in a box above the cards when the three cards are scratched.
  6. Image Width, Image Height: Any dimensions can be used but if the original image aspect-ration isn't observed there will be some distortion. The plugin does no image manipulation.
  7. Instructions: Text that is displayed above the cards, instructing the user what to do.

## Other stuff ##

* The code is maintained by Steve Clason, [steve@steveclason.com](mailto:steve@steveclason.com).
* Contact Incite Response for your digital marketing needs, [info@InciteResponse.com](mailto:info@inciteresponse.com)
