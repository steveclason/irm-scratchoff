<?php
/**
 * Sample code only see https://developer.wordpress.org/plugins/plugin-basics/uninstall-methods/
 */

// If uninstall.php is not called by WordPress, die
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
  wp_die( sprintf(
       __( '%s should only be called when uninstalling the plugin.', 'pdev' ),
       __FILE__
  ) );
  exit;
}

$option_name = 'irm_scratchoff_options';

delete_option( $option_name );
