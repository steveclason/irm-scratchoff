/**
 * IRM-Scratchoff plugin code.
 * 
 */

/* global irm_scratchoff_settings */

( function( $ ){

  // Must wait until the page is fully loaded, takes a while.
  window.onload = function() {

    // NOTE variable irm_scratchoff_settings is an array localized by WP from the plugin configuration.

    $( '#redraw' ).click( function() {

      // Clear the screen.
      $( '.scratch-card' ).each( function() {
        $( this ).wScratchPad( 'reset' );
      });
      $( '#scratch_result' ).addClass( 'hidden' ).removeClass( 'winner loser' );
      $( '#counter' ).html( '0' );
      doIt( irm_scratchoff_settings );
    });

    function announceWinner() {
      // alert ( 'Dude, you won!' );
      $( '#scratch_result' ).removeClass( 'hidden' ).addClass( 'winner' );
    }

    // Draws the cards.
    function doIt( settings ) {
      // console.log( 'Doing it.' );

      var background_image = settings.BG_URL;
      var mask_image = settings.MASK_URL;
      // var scratch_width = settings.WIDTH;
      // var scratch_height = settings.HEIGHT;
      var completion_percent = settings.PERCENT;

      var checkins = [];

      // Prevents multiple hits on callback.
      function update_scratched_count( element_id ) {
        if ( !checkins.includes( element_id )) {
          if ( checkins.push( element_id ) == 3 ) {
            announceWinner();
          }
        }
      }

      // Draw the three cards.
      $('#scratch_card_1').wScratchPad({
        bg: background_image,
        fg: mask_image,
        cursor: 'crosshair',
        scratchMove: function ( e, percent ) {
          $( '#counter' ).html( percent );
          if ( percent > completion_percent ) {
            this.clear();
            update_scratched_count( 'scratch_card_1');
            return;
          }
        },
        size: 30
      });

      $('#scratch_card_2').wScratchPad({
        bg: background_image,
        fg: mask_image,
        cursor: 'crosshair',
        scratchMove: function (e, percent) {
          $('#counter').html(percent);
          if (percent > completion_percent) {
            this.clear();
            update_scratched_count( 'scratch_card_2');
          }
        },
        size: 30
      });

      $('#scratch_card_3').wScratchPad({
        bg: background_image,
        fg: mask_image,
        cursor: 'crosshair',
        scratchMove: function (e, percent) {
          $('#counter').html(percent);
          if (percent > completion_percent) {
            this.clear();
            update_scratched_count( 'scratch_card_3');
          }
        },
        size: 30
      });
    }
    
    // Draw the initial cards.
    doIt( irm_scratchoff_settings );
  };
})(jQuery);