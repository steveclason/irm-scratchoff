<?php
/**
 * Plugin Name: InciteResponse Scratch-Off
 * Plugin URI:        https://bitbucket.org/steveclason/irm-scratchoff/src/master/
 * Description:       Provide a scratch-card feature for rewards.
 * Version:           1.0.0
 * Requires at least: 5.4
 * Requires PHP:      7.3
 * Author:            Steve Clason
 * Author URI:        https://steveclason.com/
 * License:           MIT
 * License URI:       https://mit-license.org/
 * 
 * wScratchPad.js is Copyright (C) 2011-2012 Websanova http://www.websanova.com, https://github.com/websanova/wScratchPad.
 
 * Copyright © 2020 Incite Response Marketing <https://inciteresponse.com>

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

require plugin_dir_path( __FILE__ ) . '/includes/shortcodes.php';

/**
 * Enqueue scratch-off resources.
 */

 // Register scripts on init or plugins_loaded, enqueue them later, when they're needed.
function irm_scratchoff_register_resources() {

  // Use minified version for production.
  $options = get_option( 'irm_scratchoff_options' );
  if ( $options[ 'irm_scratchoff_field_devmode' ] === 'on' ) {
    wp_register_script( 'irm-scratchoff-vendor', WP_PLUGIN_URL . '/irm-scratchoff/js/wScratchPad.js', array( 'jquery' ), filemtime( plugin_dir_path( __FILE__ ) . 'js/wScratchPad.js' ), false );
  } else {
    wp_register_script( 'irm-scratchoff-vendor', WP_PLUGIN_URL . '/irm-scratchoff/js/wScratchPad.min.js', array( 'jquery' ), filemtime( plugin_dir_path( __FILE__ ) . 'js/wScratchPad.js' ), false );
  }

  wp_register_script( 'irm-scratchoff', WP_PLUGIN_URL . '/irm-scratchoff/js/irm-scratchoff.js', array( 'irm-scratchoff-vendor', 'jquery' ), filemtime( plugin_dir_path( __FILE__ ) . 'js/irm-scratchoff.js' ), false );

  wp_register_style( 'irm-scratchoff-styles', WP_PLUGIN_URL . '/irm-scratchoff/css/irm-scratchoff-style.css', array( ), filemtime( plugin_dir_path( __FILE__ ) . 'css/irm-scratchoff-style.css' ) );
}
add_action( 'init', 'irm_scratchoff_register_resources' );

// Enqueue scripts and styles only if the page has the scratchoff shortcode.
function irm_scratchoff_enqueue_resources() {
  global $post;
  if ( is_singular() && has_shortcode( get_the_content( null, false, $post -> id ), 'irm_scratchoff' ) ) {
    wp_enqueue_script( 'irm-scratchoff-vendor' );
    wp_enqueue_style( 'irm-scratchoff-styles' );
    // 'irm-scratchoff' will be enqueued in the shortcode after variables are assigned.
  }
}
add_action( 'wp_enqueue_scripts', 'irm_scratchoff_enqueue_resources' );

/**
 * Plugin option and settings
 */

 // All of the defaults is one place to make it easy to manage.
$scratchoff_defaults = array(
  'background_url' => WP_PLUGIN_URL . '/irm-scratchoff/images/matchandwin.jpg',
  'foreground_url' => WP_PLUGIN_URL . '/irm-scratchoff/images/silver-pattern.png',
  'percent' => '50',
  'height' => '388',
  'width' => '290',
  'devmode' => 'off',
  'winner_txt' => 'Congratulations! You&apos;ve won!',
  'loser_txt' => '',
  'instructions_txt' => 'USE YOUR MOUSE (OR FINGER) TO VIRTUALLY SCRATCH ALL THREE CARDS. IF YOU MATCH ALL THREE, YOU ARE A WINNER!'
);

/**
  * To enable use of media manager
  * see https://wordpress.org/support/topic/howto-integrate-the-media-library-into-a-plugin/?replies=4
  */

// FIXME Delete this block if we don't need it.
// function wp_gear_manager_admin_scripts() {
//   wp_enqueue_script('media-upload');
//   wp_enqueue_script('thickbox');
//   wp_enqueue_script('jquery');
// }
// function wp_gear_manager_admin_styles() {
//   wp_enqueue_style('thickbox');
// }
// add_action('admin_print_scripts', 'wp_gear_manager_admin_scripts');
// add_action('admin_print_styles', 'wp_gear_manager_admin_styles');
// End media manager.

function irm_scratchoff_settings_init() {
  // register a new setting for "irm_scratchoff" page args: $option_group, $option_name, $args
  register_setting( 'irm_scratchoff', 'irm_scratchoff_options' );
  
  // register a new section in the "irm_scratchoff" page
  add_settings_section(
  'irm_scratchoff_section_controls', // id
  __( 'Scratchoff Settings', 'irm_scratchoff' ), // title
  'irm_scratchoff_section_controls_cb', // callback
  'irm_scratchoff' //page
  );

  // Add dev mode field.
  add_settings_field(
    'irm_scratchoff_field_devmode', // id
    __( 'Development Mode', 'irm_scratchoff' ), // title
    'irm_scratchoff_field_devmode_cb', // callback
    'irm_scratchoff', // page
    'irm_scratchoff_section_controls', //section
    [ // args
    'label_for' => 'irm_scratchoff_field_devmode',
    'class' => 'irm_scratchoff_row',
    'irm_scratchoff_custom_data' => 'custom',
    ]
  );

  // Add background image field.
  add_settings_field(
    'irm_scratchoff_field_background', // id
    __( 'Background Image', 'irm_scratchoff' ), // title
    'irm_scratchoff_field_background_cb', // callback
    'irm_scratchoff', // page
    'irm_scratchoff_section_controls', //section
    [ // args
    'label_for' => 'irm_scratchoff_field_background',
    'class' => 'irm_scratchoff_row',
    'irm_scratchoff_custom_data' => 'custom',
    ]
  );

  // Add foreground image field, default to silver pattern.
  // TODO Make this selectable.
  add_settings_field(
    'irm_scratchoff_field_foreground', // id
    __( 'Foreground Image', 'irm_scratchoff' ), // title
    'irm_scratchoff_field_foreground_cb', // callback
    'irm_scratchoff', // page
    'irm_scratchoff_section_controls', //section
    [ // args
    'label_for' => 'irm_scratchoff_field_foreground',
    'class' => 'irm_scratchoff_row',
    'irm_scratchoff_custom_data' => 'custom',
    ]
  );

  // Add percent field, % to be scratched to trigger complete, default to 50.
  add_settings_field(
    'irm_scratchoff_field_percent', // id
    __( 'Completion trigger %', 'irm_scratchoff' ), // title
    'irm_scratchoff_field_percent_cb', // callback
    'irm_scratchoff', // page
    'irm_scratchoff_section_controls', //section
    [ // args
    'label_for' => 'irm_scratchoff_field_percent',
    'class' => 'irm_scratchoff_row',
    'irm_scratchoff_custom_data' => 'custom',
    ]
  );

  // Add winner text field, message to be displayed in the case of a winning scratch.
  add_settings_field(
    'irm_scratchoff_field_winner', // id
    __( 'Winner Text', 'irm_scratchoff' ), // title
    'irm_scratchoff_field_winner_cb', // callback
    'irm_scratchoff', // page
    'irm_scratchoff_section_controls', //section
    [ // args
    'label_for' => 'irm_scratchoff_field_winner',
    'class' => 'irm_scratchoff_row',
    'irm_scratchoff_custom_data' => 'custom',
    ]
  );

  // Add width field, setting for the width of the image element.
  add_settings_field(
    'irm_scratchoff_field_width', // id
    __( 'Image Width', 'irm_scratchoff' ), // title
    'irm_scratchoff_field_width_cb', // callback
    'irm_scratchoff', // page
    'irm_scratchoff_section_controls', //section
    [ // args
    'label_for' => 'irm_scratchoff_field_width',
    'class' => 'irm_scratchoff_row',
    'irm_scratchoff_custom_data' => 'custom',
    ]
  );

  // Add height field, setting for the height of the image element.
  add_settings_field(
    'irm_scratchoff_field_height', // id
    __( 'Image Height', 'irm_scratchoff' ), // title
    'irm_scratchoff_field_height_cb', // callback
    'irm_scratchoff', // page
    'irm_scratchoff_section_controls', //section
    [ // args
    'label_for' => 'irm_scratchoff_field_height',
    'class' => 'irm_scratchoff_row',
    'irm_scratchoff_custom_data' => 'custom',
    ]
  );

  // Add instructions field, text for the scratch off instructions.
  add_settings_field(
    'irm_scratchoff_field_instructions', // id
    __( 'Instructions', 'irm_scratchoff' ), // title
    'irm_scratchoff_field_instructions_cb', // callback
    'irm_scratchoff', // page
    'irm_scratchoff_section_controls', //section
    [ // args
    'label_for' => 'irm_scratchoff_field_instructions',
    'class' => 'irm_scratchoff_row',
    'irm_scratchoff_custom_data' => 'custom',
    ]
  );
}

 /**
  * register our irm_scratchoff_settings_init to the admin_init action hook
  */
 add_action( 'admin_init', 'irm_scratchoff_settings_init' );

 /**
 * custom option and settings:
 * callback functions
 */
 
// controls section cb
 
// section callbacks can accept an $args parameter, which is an array.
// $args have the following keys defined: title, id, callback.
// the values are defined at the add_settings_section() function.
function irm_scratchoff_section_controls_cb( $args ) {
  ?>
  <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Configure the scratchoff and use the shortcode "[irm_scratchoff]".', 'irm_scratchoff' ); ?></p>
  <?php
 }

// Callback for devmode switch
function irm_scratchoff_field_devmode_cb( $args ) {
  $options = get_option( 'irm_scratchoff_options' );
  
  // If option is not set, fall back to default.
  global $scratchoff_defaults;
  if ( strlen( $options[ 'irm_scratchoff_field_devmode' ] ) > 0 ) { 
    $dev = $options[ 'irm_scratchoff_field_devmode' ];
  } else {
    $dev = $scratchoff_defaults[ 'devmode' ];
  }
  ?>
  <input type="checkbox" id="<?php echo esc_attr( $args['label_for'] ); ?>"
  data-custom="<?php echo esc_attr( $args['irm_scratchoff_custom_data'] ); ?>"
  name="irm_scratchoff_options[<?php echo esc_attr( $args['label_for'] ); ?>]"

  <?php
  if ( $dev === 'on' ) {
    echo 'checked="checked"';
  }
  ?>
  >
  <p class="description">When checked, a redraw button and a percentage counter are displayed and a non-minified version of wScratchPad.js is used.</p>
  <?php
}

// Callback function for scratchoff background image.
function irm_scratchoff_field_background_cb( $args ) {
  wp_enqueue_media();
  $options = get_option( 'irm_scratchoff_options' );
  ?>

  <script language="JavaScript">
    // @see https://wordpress.org/support/topic/howto-integrate-the-media-library-into-a-plugin/?replies=4 also above.
    jQuery( document ).ready( function ( $ ) {
      $( '#upload_image_button' ).on( 'click', function( e ) {
        e.preventDefault();
        var $button = $( this );

        // Create the media frame.
        var file_frame = wp.media.frames.file_frame = wp.media({
          title: 'Select of upload image.',
          library: {
            type: 'image'
          },
          button: {
            text: 'Select'
          },
          multiple: false
        });

        // When an image is selected, run a callback.
        file_frame.on( 'select', function() {
          var attachment = file_frame.state().get( 'selection' ).first().toJSON();
          $button.siblings( 'input' ).val( attachment.url ).change();
          $( '#irm_scratchoff_background_thumbnail' ).attr( 'src', attachment_url );
        });

        file_frame.open();

      });


    });
  </script>
  <div class="irm-scratchoff-background-thumbnail-wrapper" style="max-width: 100px; display: inline-block">
  <?php

  // If option is not set, fall back to default.
  global $scratchoff_defaults;
  if ( strlen( $options[ 'irm_scratchoff_field_background' ] ) > 0 ) { 
    $bg = $options[ 'irm_scratchoff_field_background' ];
  } else {
    $bg = $scratchoff_defaults[ 'background_url' ];
  }
    ?>
    <img id="irm_scratchoff_background_thumbnail" src="<?php echo $bg; ?>" style="max-width: 100%;" />

  </div>
  <input class="widefat" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="irm_scratchoff_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $bg; ?>" data-custom="<?php echo esc_attr( $args['irm_scratchoff_custom_data'] ); ?>" />
  <button id="upload_image_button">Upload image</button>
  <?php
}

// Callback for foreground image.
function irm_scratchoff_field_foreground_cb( $args ) {
  $options = get_option( 'irm_scratchoff_options' );

  // If option is not set, fall back to default.
  global $scratchoff_defaults;
  if ( strlen( $options[ 'irm_scratchoff_field_foreground' ] ) > 0 ) { 
    $fg = $options[ 'irm_scratchoff_field_foreground' ];
  } else {
    $fg = $scratchoff_defaults[ 'foreground_url' ];
  }
  ?>
  <input class="widefat" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="irm_scratchoff_options[<?php echo esc_attr( $args['label_for'] ); ?>]" data-custom="<?php echo esc_attr( $args['irm_scratchoff_custom_data'] ); ?>" value="<?php echo $fg; ?>" />
  <p class="description">
  <?php esc_html_e( 'The foreground image that will be scratched off. Defaults to /wp-content/plugins/irm-scratchoff/images/silver-pattern.png' ); ?>
  </p>
  <?php
}

// Callback function for scratchoff percent complete.
function irm_scratchoff_field_percent_cb( $args ) {
  $options = get_option( 'irm_scratchoff_options' );
  $dev = $options[ 'irm_scratchoff_field_devmode' ];

  if ( $dev === 'on' ) {
    $pc = '20';
  } else {  
    // If option is not set, fall back to default.
    global $scratchoff_defaults;
    if ( strlen( $options[ 'irm_scratchoff_field_percent' ] ) > 0 ) { 
      $pc = $options[ 'irm_scratchoff_field_percent' ];
    } else {
      $pc = $scratchoff_defaults[ 'percent' ];
    }
  }
  ?>
  <input class="" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="irm_scratchoff_options[<?php echo esc_attr( $args['label_for'] ); ?>]" data-custom="<?php echo esc_attr( $args['irm_scratchoff_custom_data'] ); ?>" value="<?php echo $pc; ?>" />
  <p class="description">
  <?php esc_html_e( 'The percentage of the image that must be scratched before it is considered complete. Set this low for testing, Default is 50%.' ); ?>
  </p>
  <?php
}

// Callback function for scratchoff image height.
function irm_scratchoff_field_height_cb( $args ) {
  $options = get_option( 'irm_scratchoff_options' );

  // If option is not set, fall back to default.
  global $scratchoff_defaults;
  if ( strlen( $options[ 'irm_scratchoff_field_height' ] ) > 0 ) { 
    $ht = $options[ 'irm_scratchoff_field_height' ];
  } else {
    $ht = $scratchoff_defaults[ 'height' ];
  }
  ?>
  <input class="" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="irm_scratchoff_options[<?php echo esc_attr( $args['label_for'] ); ?>]" data-custom="<?php echo esc_attr( $args['irm_scratchoff_custom_data'] ); ?>" value="<?php echo $ht; ?>" />
  <p class="description">
  <?php esc_html_e( 'Image height, in pixels. Stay mindful of the original aspect ratio.' ); ?>
  </p>
  <?php
}

// Callback function for scratchoff image width.
function irm_scratchoff_field_width_cb( $args ) {
  $options = get_option( 'irm_scratchoff_options' );

  // If option is not set, fall back to default.
  global $scratchoff_defaults;
  if ( strlen( $options[ 'irm_scratchoff_field_height' ] ) > 0 ) { 
    $wt = $options[ 'irm_scratchoff_field_width' ];
  } else {
    $wt = $scratchoff_defaults[ 'width' ];
  }
  ?>
  <input class="" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="irm_scratchoff_options[<?php echo esc_attr( $args['label_for'] ); ?>]" data-custom="<?php echo esc_attr( $args['irm_scratchoff_custom_data'] ); ?>" value="<?php echo $wt; ?>" />
  <p class="description">
  <?php esc_html_e( 'Image width, in pixels. Stay mindful of the original aspect ratio.' ); ?>
  </p>
  <?php
}

// Callback function for scratchoff winner text.
function irm_scratchoff_field_winner_cb( $args ) {
  $options = get_option( 'irm_scratchoff_options' );

  // If option is not set, fall back to default.
  global $scratchoff_defaults;
  if ( strlen( $options[ 'irm_scratchoff_field_winner' ] ) > 0 ) { 
    $win = $options[ 'irm_scratchoff_field_winner' ];
  } else {
    $win = $scratchoff_defaults[ 'winner_txt' ];
  }
  ?>
  <input class="widefat" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="irm_scratchoff_options[<?php echo esc_attr( $args['label_for'] ); ?>]" data-custom="<?php echo esc_attr( $args['irm_scratchoff_custom_data'] ); ?>" value="<?php echo $win; ?>" />
  <p class="description">
  <?php esc_html_e( 'The text that will be shown in the event of a winner.' ); ?>
  </p>
  <?php
}

// Callback function for instructions text.
// Default is 'USE YOUR MOUSE (OR FINGER) TO VIRTUALLY SCRATCH ALL THREE CARDS. IF YOU MATCH ALL THREE, YOU ARE A WINNER!'
function irm_scratchoff_field_instructions_cb( $args ) {
  $options = get_option( 'irm_scratchoff_options' );

  // If option is not set, fall back to default.
  global $scratchoff_defaults;
  if ( strlen( $options[ 'irm_scratchoff_field_instructions' ] ) > 0 ) { 
    $inst = $options[ 'irm_scratchoff_field_instructions' ];
  } else {
    $inst = $scratchoff_defaults[ 'instructions_txt' ];
  }
  ?>
  <input class="widefat" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="irm_scratchoff_options[<?php echo esc_attr( $args['label_for'] ); ?>]" data-custom="<?php echo esc_attr( $args['irm_scratchoff_custom_data'] ); ?>" value="<?php echo $inst; ?>" />
  <p class="description">
  <?php esc_html_e( 'The instructions for the user, shown above the cards.' ); ?>
  </p>
  <?php
}

function irm_scratchoff_options_page_html() {
  // check user capabilities
  if ( ! current_user_can( 'manage_options' ) ) {
      return;
  }

  // Add error/update messages
  // Check if the user have submitted the settings
  // WordPress will add the "settings-updated" $_GET parameter to the url
  if ( isset( $_GET[ 'settings-updated' ] ) ) {
    // add settings saved message with the class of "updated"
    add_settings_error( 'irm_scratchoff_messages', 'irm_scratchoff_message', __( 'Settings Saved', 'irm_scratchoff' ), 'updated' );
  }
    
  // show error/update messages
  settings_errors( 'irm_scratchoff_messages' );
  ?>
  
  <div class="wrap">
      <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
      <p>Use this to configure the scratchoff settings. By default there are three cards which must all be scratched to 50%. All entries are winners in this version, but all three cards must be scratched 50% for that to be announced.</p>
      <p>jQuery is required for this to run. It&apos;s bundled with WordPress but can be and sometimes is suppressed.</p>

      <form action="options.php" method="post">
          <?php
          // output security fields for the registered setting "irm_scratchoff_options"
          settings_fields( 'irm_scratchoff' );
          // output setting sections and their fields
          // (sections are registered for "irm_scratchoff", each field is registered to a specific section)
          do_settings_sections( 'irm_scratchoff' );
          // output save settings button
          submit_button( __( 'Save Settings', 'textdomain' ) );
          ?>
      </form>
  </div>
  <?php
}

// Could also use add_options_page().
function irm_scratchoff_options_page() {
  add_submenu_page(
    'options-general.php',
    'Scratchoff Options',
    'Scratchoff Options',
    'manage_options',
    'IRM Scratchoff',
    'irm_scratchoff_options_page_html'
  );
}
add_action('admin_menu', 'irm_scratchoff_options_page');