<?php

/**
 * Shortcode to fetch 1135 waiver fields.
 * Embed like <li>1135 Waivers: [shortcode_goes_here]</li>
 */
add_shortcode( 'irm_scratchoff', 'irm_scratchoff_function' );
function irm_scratchoff_function( $attributes ) {

  $atts = shortcode_atts( array(
    // default attributes.
    'post_id' => null
  ), $attributes );

  $options = get_option( 'irm_scratchoff_options' );

  $background_image_url = $options[ 'irm_scratchoff_field_background' ];
  $mask_url = $options[ 'irm_scratchoff_field_foreground' ];
  $image_width = $options[ 'irm_scratchoff_field_width' ];
  $image_height = $options[ 'irm_scratchoff_field_height' ];
  $winner_text = $options[ 'irm_scratchoff_field_winner' ];
  $loser_text = $options[ 'irm_scratchoff_field_loser' ];
  $instruction_text = $options[ 'irm_scratchoff_field_instructions' ];
  $dev_mode = $options[ 'irm_scratchoff_field_devmode' ];
  $trigger_percent = $options[ 'irm_scratchoff_field_percent' ];

  // Load script after option variables are localized to JavaScript.
  if ( wp_script_is( 'irm-scratchoff', 'registered' ) ) {
    wp_localize_script(
      'irm-scratchoff',          // Script handle. 
      'irm_scratchoff_settings',        // Name of JavaScript object
      array(                            // Array of properties for JS object
        'BG_URL' => $background_image_url,
        'WIDTH'   => $image_width,
        'MASK_URL' => $mask_url,
        'HEIGHT' => $image_height,
        'WINNER_TEXT' => $winner_text,
        'LOSER_TEXT' => $loser_text,
        'INSTRUCTION_TEXT' => $instruction_text,
        'PERCENT' => $trigger_percent
      )
    );
    wp_enqueue_script( 'irm-scratchoff' );
  } else {
    die( 'The script "irm-scratchoff" is not registered.' );
  }

  $html = '';

  if ( $dev_mode === 'on' ) {
    $html .= "<div><button id='redraw' class='redraw'>Reset</button>
    <span id='counter'>0</span><span>%</span></div>";
  }

  $card_style = 'style="width:' . $image_width . 'px;height:' . $image_height . 'px;"';

  $html .= "
  <div class='scratch-card-container'>
    <p>$instruction_text</p>
    <p id='scratch_result' class='scratch-result hidden'>$winner_text</p>
    <div id='scratch-winner'></div>
    <div class='scratch-cards' id='scratch_cards'>
      <div id='scratch_card_1' class='scratch-card' $card_style></div>
      <div id='scratch_card_2' class='scratch-card' $card_style></div>
      <div id='scratch_card_3' class='scratch-card' $card_style></div>
        
    </div>
  </div>";

  return $html;
}
